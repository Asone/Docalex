# Guide légistique

## Résumé 

Ce document présente l'ensemble des règles d'écriture juridique afin de constituer une loi en vue de la déposition d'un projet de loi.

## Source 

[http://www.senat.fr/fileadmin/Fichiers/Images/role/seance/guides_pratiques/PDF/guide_legistique_revu2013.pdf](http://www.senat.fr/fileadmin/Fichiers/Images/role/seance/guides_pratiques/PDF/guide_legistique_revu2013.pdf)

## Auteur(s)

Sénat

## Mots-clés

Sénat, Guide, règles d'écriture