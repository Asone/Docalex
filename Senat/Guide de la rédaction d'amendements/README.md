# Guide de la rédaction d'amendements, sous-amendements et motions de procédure

## Résumé

Ce document présente les méthodologie à appliquer afin de rédiger des amendements, des sous-amendements ainsi que les procédures à suivre afin de les soumettre. 

## Auteur(s)

Sénat

## Mots-clés

Sénat, Guide, Amendements, Sous-amendement, PJL, procédures