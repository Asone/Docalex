# Les nouveaux outils du travail interministériel

## Résumé

Document de présentation des outils logiciels développés sur les dernières années (jusqu'à 2017) afin de faciliter le travail des ministères et leurs coordinations. 

## Source 

[https://fr.slideshare.net/cottin/les-nouveaux-outils-du-travail-interministriel](https://fr.slideshare.net/cottin/les-nouveaux-outils-du-travail-interministriel)

## Auteur

Stéphane Cottin