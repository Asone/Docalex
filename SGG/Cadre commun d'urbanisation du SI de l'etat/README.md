# Cadre commun d'urbanisation du SI de l'etat

## Résumé 

Document produit lors d'une présentation par `Stéphane Cottin` auprès de l'ADIJ

## Source 

[http://references.modernisation.gouv.fr/sites/default/files/Cadre%20Commun%20d'Urbanisation%20du%20SI%20de%20l'Etat%20v1.0_0.pdf](http://references.modernisation.gouv.fr/sites/default/files/Cadre%20Commun%20d'Urbanisation%20du%20SI%20de%20l'Etat%20v1.0_0.pdf)

## Auteur(s)

Stéphane Cottin

