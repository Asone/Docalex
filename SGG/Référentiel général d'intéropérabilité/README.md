# Référentiel général d'intéropérabilité

## Résumé 

Document de description des différents aspects techniques visant à garantir l'intéropérabilité des systèmes d'informations entre les institutions de gouvernance de la république française

### mots-clés 

Intéropérabilité, protocoles, formats, RFC, ministères, nomenclature