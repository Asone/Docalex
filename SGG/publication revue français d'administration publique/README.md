# Extraqual : L'extranet de la qualité et de la simplification du droit

## Résumé 

Extrait d'article paru au sein de la revue française d'administration publique (Revue interne de l'ENA) sur la mise en place d'un extranet relatif au travail autour du droit. 

>  Cet extranet met à disposition de l’ensemble de l’administration centrale des tableaux de suivi, des guides de bonnes pratiques et des espaces d’information sur les diverses étapes de l’activité normative

## Source 

[https://www.cairn.info/revue-francaise-d-administration-publique-2013-2-page-313.htm?contenu=resume](https://www.cairn.info/revue-francaise-d-administration-publique-2013-2-page-313.htm?contenu=resume)

## Auteur 

Stéphane Cottin

