# (WIP) Docalex

## Présentation 

Docalex est un dépot de fichiers, notes et synthèses des différents documents permettant de comprendre comment se construisent, se traitent et se transmettent les données qui servent à la constitution des corpus de textes législatifs. 

L'idée est ici de centraliser les documents produits par les institutions publiques sur le sujet mais également de permettre à ceux qui le souhaiteraient de produire des 
documents complémentaires facilitant la compréhension des dites informations, celles-ci n'étant pas toujours évidentes. 

## Structure

Chaque document fait l'objet d'un dossier portant le nom de l'intitulé du document et contient le dit document ainsi qu'une courte note explicative mentionnant : 

- Le résumé du document
- L'URL source du document
- L'auteur du document
- Les mots clés associés au document (pour faciliter leur accès via les moteurs de recherche)

## Contributions 

Pour rajouter un document tiers ou proposer une note il vous suffit de cloner le dépôt actuel, créer une nouvelle branche sur votre dépôt, d'y pusher les documents
que vous souhaitez ajouter puis d'ouvrir une pull request afin de soumettre le nouveau document. 